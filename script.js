//Viajante
function Traveler(name, food, isHealty) {
    this.name = name;
    this.food = 1;
    this.isHealty = true;
}

Traveler.prototype.hunt = function () {
    this.food = this.food + 2;
};

Traveler.prototype.eat = function () {
    if (this.food <= 0) {
        this.isHealty = false;
    } else {
        this.food = this.food - 1;
    }
};
//////////////////////////////////
// Vagão (Carroça)
function Wagon(capacity) {
    this.capacity = capacity;
    this.passengers = [];
}

Wagon.prototype.getAvailableSeatCount = function () {
    return this.capacity - this.passengers.length;
};

Wagon.prototype.join = function (traveler) {
    if (this.getAvailableSeatCount() > 0) {
        this.passengers.push(traveler);
    }
};

Wagon.prototype.shouldQuarantine = function () {
    for (let i = 0; i < this.passengers.length; i++) {
        if (this.passengers[i].isHealty === false)
            return true;
    }
    return false;
};

Wagon.prototype.totalFood = function () {
    let total = 0;
    this.passengers.forEach((traveler) => {
        total = total + traveler.food;
    });

    return total;
};



//Criar uma carroça que comporta 2 pessoas
let wagon = new Wagon(2);
// Criar três viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');


console.log(`${wagon.getAvailableSeatCount()} should be 2`);

wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);

wagon.join(juan);
wagon.join(maude); // Não tem espaço para ela!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);

henrietta.hunt(); // pega mais comida
juan.eat();
juan.eat(); // juan agora está com fome (doente)

console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);
